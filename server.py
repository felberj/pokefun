from flask import Flask, g, request
from settings import telegram_access_token, pokemon_access_token, database_loc
import sqlite3

from telegram import Telegram
from database import Database

app = Flask(__name__)


@app.route("/" + telegram_access_token, methods=['POST'])
def handle_telegram():
    data = request.get_json(force=True)
    t = Telegram(telegram_access_token)
    db = Database(get_db())
    t.handle_update(db, data)
    return "Ok"


@app.route("/" + pokemon_access_token, methods=['POST'])
def handle_new_pokemon():
    data = request.form
    if not 'pokemon_name' in data:
        return "Invalid Pokemon pokemon_name"

    if not 'pokemon_id' in data:
        return "Invalid Pokemon pokemon_id"

    if not 'latitude' in data:
        return "Invalid Pokemon latitude"

    if not 'longitude' in data:
        return "Invalid Pokemon longitude"

    if not 'encounter_id' in data:
        return "Invalid Pokemon encounter_id"

    if not 'disappear_time' in data:
        return "Invalid Pokemon disappear_time"

    t = Telegram(telegram_access_token)
    db = Database(get_db())

    if db.has_handled_encounter(data['encounter_id']):
        return "Already handled"
    db.handle_encounter(data['encounter_id'])
    channels = db.get_channels_interested_in(int(data['pokemon_id']))
    for c in channels:
        t.send_message(c, "Le wild %s [%s] spawned (until %s)!" % (data['pokemon_name'], data['pokemon_id'], data['disappear_time']))
        t.send_location(c, data['latitude'], data['longitude'])
    return "Ok!"


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(database_loc)
    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


if __name__ == "__main__":
    app.run()
