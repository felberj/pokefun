from server import app

CERT = '/home/bot/secret/cert.pem'
CERT_KEY = '/home/bot/secret/privkey.pem'

if __name__ == "__main__":
    context = (CERT, CERT_KEY)
    app.run(host='0.0.0.0', port=8443, ssl_context=context)
