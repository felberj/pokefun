import re
import requests
from settings import telegram_access_token, default_pokemon
import json
from database import Database


class Telegram:
    def __init__(self, telegram_token):
        self.token = telegram_token
        self.base_url = 'https://api.telegram.org/bot%s' % telegram_token

    def send_location(self, chat_id, lat, lng):
        loc_url = self.base_url + '/sendLocation'
        data = {
            'chat_id': chat_id,
            'latitude': lat,
            'longitude': lng
        }
        
        requests.post(loc_url, data)

    def send_message(self, chat_id, message):
        loc_url = self.base_url + '/sendMessage'
        data = {
            'chat_id': chat_id,
            'text': message
        }
        requests.post(loc_url, data)

    def get_update(self, offset=0):
        loc_url = self.base_url + '/getUpdates'
        data = {
            'offset': offset
        }
        return json.loads(requests.post(loc_url, data).content)['result']

    def parse_command(self, database, chat_id, text):
        print(text)
        if text.startswith('/start'):
            self.send_start(database, chat_id)
        elif text == '/reset':
            self.send_reset(database, chat_id)
        elif text == '/help':
            self.send_help(chat_id)
        elif text == '/list':
            self.send_list(database, chat_id)
        elif text.startswith('/add '):
            self.send_add(database, chat_id, text)
        elif text.startswith('/remove '):
            self.send_remove(database, chat_id, text)

    def send_add(self, database, chat_id, text):
        num = re.findall('^\/add (\d{1,3})$', text)
        chat = database.find_by_chatid(chat_id)
        if num:
            num = int(num[0])
            if not num in chat['interested']:
                chat['interested'].append(num)
                database.update(chat)
                self.send_message(chat_id, 'Added %s to your list' % num)
            else:
                self.send_message(chat_id, '%s is already in your list' % num)
        else:
            self.send_message(chat_id, 'Error, expected /add <num>')

    def send_remove(self, database, chat_id, text):
        num = re.findall('^\/remove (\d{1,3})$', text)
        chat = database.find_by_chatid(chat_id)
        if num:
            num = int(num[0])
            if num in  chat['interested']:
                chat['interested'].remove(num)
                database.update(chat)
            self.send_message(chat_id, 'Removed %s from your list' % num)
        else:
            self.send_message(chat_id, 'Error, expected /remove <num>')

    def send_list(self, database, chat_id):
        chat = database.find_by_chatid(chat_id)
        if chat:
            message = "Pokemons on your list:\n"
            message += ", ".join(map(lambda x: str(x), chat['interested']))
            self.send_message(chat_id, message)
        else:
            self.send_message(chat_id, "No data stored for you")

    def send_reset(self, database, chat_id):
        database.insert(chat_id, default_pokemon)
        self.send_message(chat_id, "Inserted the default list of pokemon for you!")

    def send_start(self, database, chat_id):
        self.send_help(chat_id)
        chat = database.find_by_chatid(chat_id)
        if chat:
            self.send_message(chat_id, "Your list is already initialised, enter /reset to reset your default pokemons")
        else:
            self.send_reset(database, chat_id)

    def send_help(self, chat_id):
        text = ("Pokemon Locator at CAB\n"
                "==================\n"
                "see http://pokefun.felberj.ch:5000 for the whole map\n"
                "/help -> this help message\n"
                "/list -> list all pokemon you get notifications\n"
                "/add <pokemon nr> -> adds a pokemon to your notification list\n"
                "/remove <pokemon nr> -> removes a pokemon from your notification list"
                )
        self.send_message(chat_id, text)

    def handle_update(self, database, u):
        if 'message' in u and 'entities' in u['message']:
            if u['message']['entities'][0]['type'] == 'bot_command':
                id = u['message']['chat']['id']
                text = u['message']['text']
                self.parse_command(database, id, text)
