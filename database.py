import numbers

col_poke_prefix = 'poke_'
max_pokemon = 151
class Database:
    def __init__(self, sqlite_conn):
        self.conn = sqlite_conn
        all_pokemon = ' text, '.join(map(lambda x: col_poke_prefix + str(x), xrange(1, max_pokemon + 1))) + ' text'
        c = self.conn.cursor()
        c.execute('CREATE TABLE IF NOT EXISTS users (chat_id text, %s)' % all_pokemon)
        c.execute('CREATE TABLE IF NOT EXISTS encounters (encounter_id text)')
        self.conn.commit()

    def has_handled_encounter(self, encounter_id):
        c = self.conn.cursor()
        curs = c.execute("SELECT encounter_id FROM encounters WHERE encounter_id = ?", (encounter_id, ))
        res = curs.fetchone()
        return bool(res)

    def handle_encounter(self, encounter_id):
        if not self.has_handled_encounter(encounter_id):
            c = self.conn.cursor()
            c.execute("INSERT INTO encounters VALUES (?)", (encounter_id, ))
            self.conn.commit()

    def get_channels_interested_in(self, poke_id):
        if not isinstance(poke_id, numbers.Number):
            print('ERROR: Pokeid not a number')
            return
        c = self.conn.cursor()
        curs = c.execute("SELECT chat_id FROM users WHERE %s = 1" % (col_poke_prefix + str(poke_id)))
        res = curs.fetchall()
        return map(lambda x: x[0], res)

    def find_by_chatid(self, chat_id):
        c = self.conn.cursor()
        cur = c.execute('SELECT * FROM users WHERE chat_id = ?', (chat_id,))
        res = cur.fetchone()
        #res = filter(lambda x: x['chat_id'] == chat_id, self.data)
        if res:
            interest = []
            for i in xrange(1, max_pokemon + 1):
                if res[i] == '1':
                    interest.append(i)

            return {
                'chat_id' : res[0],
                'interested' : interest
            }
        return None

    def update(self, entry):
        if not entry['chat_id']:
            return

        old = self.find_by_chatid(entry['chat_id'])
        interests = tuple(map(lambda x: x in entry['interested'], xrange(1, max_pokemon + 1)))
        c = self.conn.cursor()
        if old:
            data = interests + (entry['chat_id'], )
            sql = ' , '.join(map(lambda x: col_poke_prefix + str(x) + ' = ?', xrange(1, max_pokemon + 1)))
            c.execute('UPDATE users SET %s WHERE chat_id = ?' % sql, data)
        else:
            data = (entry['chat_id'], ) + interests
            sql = " , ".join(['?'] * max_pokemon)
            c.execute('INSERT INTO users VALUES (?, %s)' % sql, data)
        self.conn.commit()

    def insert(self, chat_id, interests):
        self.update({
            'chat_id': chat_id,
            'interested': interests
        })
